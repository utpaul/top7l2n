"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var user_1 = require("../mapper/user");
var menu_1 = require("../constants/menu");
var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, auth, networkCheck, eventCtrl) {
        var _this = this;
        this.platform = platform;
        this.auth = auth;
        this.networkCheck = networkCheck;
        this.eventCtrl = eventCtrl;
        this.isAuthenticated = false;
        this.pages = [];
        this.initializeApp(statusBar, splashScreen);
        menu_1.APP_MENUS.sidebar.forEach(function (el) {
            _this.pages.push({ title: el.label, icon: el.icon, component: el.component });
        });
    }
    MyApp.prototype.initializeApp = function (statusBar, splashScreen) {
        var _this = this;
        this.platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
            _this.auth.onSessionStore.subscribe(function () {
                if (_this.auth.isAuthenticated == true) {
                    _this.isAuthenticated = true;
                    _this.user = new user_1.User(_this.auth.currentUser);
                    _this.nav.setRoot('Home');
                }
                else {
                    _this.nav.setRoot('Home').then(function () {
                        _this.isAuthenticated = false;
                    });
                }
            });
            _this.eventCtrl.subscribe('network:online', function () {
            });
            _this.eventCtrl.subscribe('network:offline', function () {
            });
        });
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.push(page.component);
    };
    return MyApp;
}());
__decorate([
    core_1.ViewChild(ionic_angular_1.Nav)
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    core_1.Component({
        templateUrl: 'app.html'
    })
], MyApp);
exports.MyApp = MyApp;

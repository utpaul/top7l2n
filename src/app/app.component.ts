import {Component, ViewChild} from '@angular/core';
import {Platform, Events,Nav} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {User} from "../mapper/user";
import {AuthProvider} from "../providers/auth";
import {NetworkConnectionProvider} from "../providers/networkConnection";
import {APP_MENUS} from "../constants/menu";
import {MemberManagementProvider} from "../providers/memberManagement";
import {L2nHttp} from "../providers/l2n-http";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  isAuthenticated: boolean = false;
  user: User;
  rootPage: any;
  pages: Array<{title: string, icon: string, component: any, permission:string}> = [];

  constructor(public platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private auth:AuthProvider,
              private networkCheck:NetworkConnectionProvider,
              private eventCtrl: Events,
              private memberManagementProvider:MemberManagementProvider,
              private l2nHttp:L2nHttp) {

    this.initializeApp(statusBar, splashScreen);

  }

  initializeApp(statusBar: StatusBar,
                splashScreen: SplashScreen) {
    this.platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.auth.onSessionStore.subscribe(() => {

        if (this.auth.isAuthenticated == true) {
          this.isAuthenticated = true;
          this.user = new User(this.auth.currentUser);
          this.nav.setRoot('home-page', {flag: 1}).then(() => {

            APP_MENUS.sidebar.forEach(el => {

              if(el.permission == 'Admin' && this.user.authority == 'Admin'){
                this.pages =[];

                this.pages.push({title: el.label,
                  icon: el.icon, component: el.component,permission:el.permission});
              }
              else if(el.permission == this.user.authority || el.permission == 'Member'){

                if(el.label == 'Select '){

                  if( this.user.authority != 'Admin'){
                    this.pages.push({title: el.label+ this.user.calculation+' Team Member' ,
                      icon: el.icon, component: el.component,permission:el.permission});
                  }

                }else {

                  this.pages.push({title: el.label, icon: el.icon,
                    component: el.component,permission:el.permission});
                }
              }
            });

          });

        } else {
            this.nav.setRoot('login-page').then(() => {
            this.isAuthenticated = false;
          });
        }

      });

      this.eventCtrl.subscribe('network:online', () => {

      });

      this.eventCtrl.subscribe('network:offline', () => {

      });
    });
  }

  openPage(page) {
    this.nav.push(page.component);
  }

  logout() {

    this.nav.setRoot('login-page').then(() => {
      this.auth.destroyUserCredentials();
      this.memberManagementProvider.removeAllLists();
      this.pages=[];
    });
  }

}


"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var splash_screen_1 = require("@ionic-native/splash-screen");
var status_bar_1 = require("@ionic-native/status-bar");
var network_1 = require("@ionic-native/network");
var screen_orientation_1 = require("@ionic-native/screen-orientation");
var app_component_1 = require("./app.component");
var auth_1 = require("../providers/auth");
var http_1 = require("@angular/http");
var networkConnection_1 = require("../providers/networkConnection");
var block_ui_1 = require("../providers/block-ui");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            app_component_1.MyApp
        ],
        imports: [
            http_1.HttpModule,
            platform_browser_1.BrowserModule,
            ionic_angular_1.IonicModule.forRoot(app_component_1.MyApp)
        ],
        bootstrap: [ionic_angular_1.IonicApp],
        entryComponents: [
            app_component_1.MyApp
        ],
        providers: [
            status_bar_1.StatusBar,
            splash_screen_1.SplashScreen,
            { provide: core_1.ErrorHandler, useClass: ionic_angular_1.IonicErrorHandler },
            screen_orientation_1.ScreenOrientation,
            network_1.Network,
            auth_1.AuthProvider,
            block_ui_1.BlockUi,
            networkConnection_1.NetworkConnectionProvider
        ]
    })
], AppModule);
exports.AppModule = AppModule;

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Network } from '@ionic-native/network';
import {ScreenOrientation} from "@ionic-native/screen-orientation";

import { MyApp } from './app.component';
import {AuthProvider} from "../providers/auth";
import {HttpModule} from "@angular/http";
import {NetworkConnectionProvider} from "../providers/networkConnection";
import {BlockUi} from "../providers/block-ui";
import {L2nHttp} from "../providers/l2n-http";
import {MemberManagementProvider} from "../providers/memberManagement";

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],

  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthProvider,
    BlockUi,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ScreenOrientation,
    Network,
    NetworkConnectionProvider,
    L2nHttp,
    MemberManagementProvider
  ]
})
export class AppModule {}

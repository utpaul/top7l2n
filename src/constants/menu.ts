export const APP_MENUS = {
    sidebar: [
      {
        label: 'Select 7 L2n Member',
        cache: false,
        icon: 'md-contacts',
        component: 'Select7member',
        permission:'Member'
      },
      {
        label: 'Select ',
        cache: false,
        icon: 'md-people',
        component: 'SelectXnmember',
        permission:'Member'
      },
      {
        label: 'Apply TL Points',
        cache: false,
        icon: 'md-hand',
        component: 'ApplyTLPoints',
        permission:'Team Head'
      },
      {
        label: 'Apply MDs Points',
        cache: false,
        icon: 'md-hand',
        component: 'ApplyMDPoints',
        permission:'Admin'
      }
    ]
};

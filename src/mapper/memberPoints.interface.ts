export interface MemberPoints{
  id:string,
  full_name:string,
  email:string,
  photo:string,
  designation:string,
  dept_name:string,
  points:number
}

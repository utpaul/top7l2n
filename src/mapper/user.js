"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = (function () {
    function User(parameters) {
        var id = parameters.id, full_name = parameters.full_name, profile = parameters.profile, designation = parameters.designation, department = parameters.department, authority = parameters.authority, calculation = parameters.calculation;
        this._id = id;
        this._full_name = full_name;
        this._profile = profile;
        this._designation = designation;
        this._department = department;
        this._authority = authority;
        this._calculation = calculation;
    }
    Object.defineProperty(User.prototype, "calculation", {
        get: function () {
            return this._calculation;
        },
        set: function (value) {
            this._calculation = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "designation", {
        get: function () {
            return this._designation;
        },
        set: function (value) {
            this._designation = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "department", {
        get: function () {
            return this._department;
        },
        set: function (value) {
            this._department = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "authority", {
        get: function () {
            return this._authority;
        },
        set: function (value) {
            this._authority = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "name", {
        get: function () {
            return this._full_name;
        },
        set: function (value) {
            this._full_name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "profile", {
        get: function () {
            return this._profile;
        },
        set: function (value) {
            this._profile = value;
        },
        enumerable: true,
        configurable: true
    });
    return User;
}());
exports.User = User;

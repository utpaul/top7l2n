export class User {

  private _id:number;
  private _full_name:string;
  private _profile:string;
  private _designation:string;
  private _department:string;
  private _authority:string;
  private _calculation:string;


  constructor(parameters: {id?: number, full_name?: string,profile?: string,
    designation?:string,department?:string, authority?:string, calculation?:string}) {
    let {id, full_name, profile,designation,department,authority,calculation} = parameters;
    this._id = id;
    this._full_name = full_name;
    this._profile = profile;
    this._designation = designation;
    this._department=department;
    this._authority= authority;
    this._calculation = calculation;
  }

  get calculation(): string {
    return this._calculation;
  }

  set calculation(value: string) {
    this._calculation = value;
  }

  set designation(value: string) {
    this._designation = value;
  }

  set department(value: string) {
    this._department = value;
  }

  set authority(value: string) {
    this._authority = value;
  }

  get designation(): string {
    return this._designation;
  }

  get department(): string {
    return this._department;
  }

  get authority(): string {
    return this._authority;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._full_name;
  }

  set name(value: string) {
    this._full_name = value;
  }

  get profile(): string {
    return this._profile;
  }

  set profile(value: string) {
    this._profile = value;
  }
}

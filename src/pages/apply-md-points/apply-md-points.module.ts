import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApplyMDPoints } from './apply-md-points';

@NgModule({
  declarations: [
    ApplyMDPoints,
  ],
  imports: [
    IonicPageModule.forChild(ApplyMDPoints),
  ],
  exports: [
    ApplyMDPoints
  ]
})
export class ApplyMDPointsModule {}

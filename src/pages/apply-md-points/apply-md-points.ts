import { Component } from '@angular/core';
import { IonicPage} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-apply-md-points',
  templateUrl: 'apply-md-points.html',
})
export class ApplyMDPoints {

  tab1:any;
  tab2:any;

  constructor() {
    this.tab1='ListOfAdminMembers';
    this.tab2='SelectedAdminMembers';
  }
}

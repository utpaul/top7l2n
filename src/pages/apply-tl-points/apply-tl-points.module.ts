import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApplyTLPoints } from './apply-tl-points';

@NgModule({
  declarations: [
    ApplyTLPoints,
  ],
  imports: [
    IonicPageModule.forChild(ApplyTLPoints),
  ],
  exports: [
    ApplyTLPoints
  ]
})
export class ApplyTLPointsModule {}

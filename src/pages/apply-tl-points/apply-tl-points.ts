import { Component } from '@angular/core';
import { IonicPage} from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-apply-tl-points',
  templateUrl: 'apply-tl-points.html',
})
export class ApplyTLPoints {

  tab1:any;
  tab2:any;

  constructor() {
    this.tab1='ListOfTeamLeaderMembers';
    this.tab2='SelectedTeamLeaderMembers';
  }

}

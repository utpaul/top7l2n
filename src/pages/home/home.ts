import { Component } from '@angular/core';
import {IonicPage, Platform, NavParams} from 'ionic-angular';
import {L2nHttp} from "../../providers/l2n-http";
import {API_CONFIG} from "../../constants/api";
import {AuthProvider} from "../../providers/auth";
import {MemberManagementProvider} from "../../providers/memberManagement";

@IonicPage({
  name: 'home-page',
  priority: 'high'
})

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class Home {

  private nominatedPersonList =[];
  private nominatedPersonSelectedList =[];
  private imageUrl:any;
  private loadListData =[];
  exchangeFlag:any;
  private flagPrams:number;

  constructor(private l2nHttp:L2nHttp,
              public platform: Platform,
              private auth:AuthProvider,
              public navParams: NavParams,
              private memberManagementProvider:MemberManagementProvider) {

    this.platform.ready().then(() => {

      this.imageUrl =API_CONFIG.misBase;
      this.flagPrams = navParams.get("flag");

      if(this.flagPrams == 1){

        this.loadData();

      }else {

        this.loadData();

      }
    });

  }

  // getData(){
  //
  //   if(this.auth.currentUser.authority =='Admin'){
  //
  //     this.setSelectedAdminPointsMember();
  //
  //   }else if(this.auth.currentUser.authority =='Member'){
  //
  //     this.setSelectedMember();
  //     this.setSelectedTeamMember();
  //
  //   }else if(this.auth.currentUser.authority =='Team Head'){
  //
  //     this.setSelectedMember();
  //     this.setSelectedTeamMember();
  //     this.setSelectedTLPointsMember();
  //
  //   }
  // }
  //
  // setSelectedMember(){
  //
  //   this.l2nHttp.getRequest('get-7l2n-members','loading data...').then(
  //     data => {
  //       for(let i=0 ; i<data.length; i++){
  //         this.memberManagementProvider.addToSelectedMember(data[i] as Member,1,1);
  //       }
  //     },
  //     err => {}
  //   );
  //
  // }
  //
  // setSelectedTeamMember(){
  //
  // }
  //
  // setSelectedAdminPointsMember(){
  //
  // }
  //
  // setSelectedTLPointsMember(){
  //
  // }

  loadData(){

     this.l2nHttp.getRequest('nominated-person-list','loading data...').then(
      data => {

        this.loadListData =data;

        for(var j=0;j < 9;j++){

          if(parseInt(this.loadListData[j].total) == parseInt(this.loadListData[j+1].total)){

            var firstData = new Date(this.loadListData[j].join_date);
            var secondData= new Date(this.loadListData[j+1].join_date);

            if(firstData>secondData){

              this.exchangeFlag =this.loadListData[j+1];
              this.loadListData[j+1] = this.loadListData[j];
              this.loadListData[j] =this.exchangeFlag;

            }else {

            }

          }

          this.nominatedPersonSelectedList.push(this.loadListData[j]);

          if(j == 8){
            this.nominatedPersonSelectedList.push(this.loadListData[j+1]);
          }

        }

        if(this.auth.currentUser.authority =='Admin'){

          this.nominatedPersonList =this.nominatedPersonSelectedList;

        }else {

          for(var i=0;i<7;i++){
            this.nominatedPersonList.push(this.nominatedPersonSelectedList[i]);
          }

        }

      },
      err => {}
    );
  }


}

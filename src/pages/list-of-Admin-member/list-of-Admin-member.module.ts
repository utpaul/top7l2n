import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListOfAdminMembers } from './list-of-Admin-member';

@NgModule({
  declarations: [
    ListOfAdminMembers,
  ],
  imports: [
    IonicPageModule.forChild(ListOfAdminMembers),
  ],
  exports: [
    ListOfAdminMembers
  ]
})
export class ListOfTeamMembersModule {}

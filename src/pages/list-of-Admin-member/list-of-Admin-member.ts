import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {API_CONFIG} from "../../constants/api";
import {L2nHttp} from "../../providers/l2n-http";
import {MemberManagementProvider} from "../../providers/memberManagement";
import {BlockUi} from "../../providers/block-ui";
import {MemberPoints} from "../../mapper/memberPoints.interface";

@IonicPage()
@Component({
  selector: 'list-of-admin-member',
  templateUrl: 'list-of-Admin-member.html',
})
export class ListOfAdminMembers {

  private loadedList: MemberPoints[] =[];
  private imageUrl:any;
  private counter: number = 0;
  private loadCollectedData: MemberPoints[] =[];
  private flagList:any=[];

  constructor(private l2nHttp:L2nHttp,
              private memberManagementProvider:MemberManagementProvider,
              private blockUi: BlockUi,
              public navCtrl: NavController) {
    this.imageUrl =API_CONFIG.misBase;
  }

  ionViewDidLoad() {
    this.loadData();
  }

  ionViewWillEnter() {

    if(this.loadedList.length !=0){

      this.loadCollectedData =this.memberManagementProvider.getSelectedAdminPointsMember();

      if(this.loadCollectedData.length == 0) {

        for (let i = 0; i < this.loadedList.length; i++) {
          this.loadedList[i].points = 0;
        }
      }else{

        let checkValue:any=[];
        this.flagList = [];

        for (let i = 0; i < this.loadedList.length; i++) {

          for (let j = 0; j < this.loadCollectedData.length; j++) {

            if (this.loadCollectedData[j].email == this.loadedList[i].email) {
              this.flagList.push(this.loadCollectedData[j].email);
            }
          }
          checkValue.push(this.loadedList[i].email);

        }

        console.log(checkValue);
        console.log(this.flagList);

        let missing = checkValue.filter(item => this.flagList.indexOf(item) < 0);

        for(let i=0;i<missing.length;i++){
          for(let j=0; j<this.loadedList.length;j++){
            if(missing[i] == this.loadedList[j].email){
              this.loadedList[j].points =0;
            }
          }
        }
      }

    }

  }

  standing(){
    this.navCtrl.push('home-page');
  }

  private loadData() {
    this.l2nHttp.getRequest('users-list','loading data...').then(
      data => {
        this.loadCollectedData =this.memberManagementProvider.getSelectedAdminPointsMember();


        if(this.loadCollectedData.length == 0){
          for(let i=0 ; i<data.length; i++){
            data[i].points = 0;
            this.loadedList.push(data[i]);
          }
        }else{

          let memberList:any=[];
          let containerData:any=[];

          for(let i=0;i<data.length;i++){
            memberList.push(data[i].email);
          }

          for(let i=0;i<this.loadCollectedData.length;i++){
            containerData.push(this.loadCollectedData[i].email);
          }

          let missing = memberList.filter(item => containerData.indexOf(item) < 0);

          for(let j=0;j<data.length;j++){
            for(let i=0;i< this.loadCollectedData.length;i++){
              if(this.loadCollectedData[i].email == data[j].email){
                data[j].points= this.loadCollectedData[i].points;
                this.loadedList.push(data[j]);
              }
            }
          }

          for(let i=0;i<missing.length;i++){
            for(let j=0;j<data.length;j++){

              if(data[j].email == missing[i]){
                data[j].points = 0;
                this.loadedList.push(data[j]);
              }
            }
          }

        }
      },
      err => {}
    );
  }

  onRemoveToSelectList(member:MemberPoints){

    this.memberManagementProvider.removeToSelectedSpecialMember(member,2);

    this.loadCollectedData =this.memberManagementProvider.getSelectedAdminPointsMember();

    if(this.loadCollectedData.length == 0){

      for(let i=0;i<this.loadedList.length;i++){
        this.loadedList[i].points =0;
      }

    }else {

      let checkValue:any=[];
      this.flagList = [];

      for (let i = 0; i < this.loadedList.length; i++) {

        for (let j = 0; j < this.loadCollectedData.length; j++) {

          if (this.loadCollectedData[j].email == this.loadedList[i].email) {
            this.flagList.push(this.loadCollectedData[j].email);
          }
        }
        checkValue.push(this.loadedList[i].email);

      }

      console.log(checkValue);
      console.log(this.flagList);

      let missing = checkValue.filter(item => this.flagList.indexOf(item) < 0);

      for(let i=0;i<missing.length;i++){
        for(let j=0; j<this.loadedList.length;j++){
          if(missing[i] == this.loadedList[j].email){
            this.loadedList[j].points =0;
          }
        }
      }

    }

  }

  onAddToSelectList(member:MemberPoints){

    this.loadCollectedData =this.memberManagementProvider.getSelectedAdminPointsMember();

    console.log(this.loadCollectedData);
    console.log(this.loadCollectedData.length);

    if(this.loadCollectedData.length > 0){

      for(let i=0;i<this.loadCollectedData.length;i++){
        this.counter +=this.loadCollectedData[i].points;
      }

    } else{
      this.counter++;
    }

    console.log(this.counter);

    if(this.counter < 5){
      member.points++;
      console.log('member points'+ member.points);
      this.memberManagementProvider.addToSelectedMember(member,4,member.points);
    }else {
      this.blockUi.alert('You can not select more then 5 user','User Attention!');
    }

    this.counter=0;

  }


}

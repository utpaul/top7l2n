"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var api_1 = require("../../constants/api");
var ListOfMembers = (function () {
    function ListOfMembers(l2nHttp, memberManagementProvider) {
        this.l2nHttp = l2nHttp;
        this.memberManagementProvider = memberManagementProvider;
        this.loadedList = [];
        this.imageUrl = api_1.API_CONFIG.misBase;
    }
    ListOfMembers.prototype.ionViewDidLoad = function () {
        this.loadData();
        console.log();
    };
    ListOfMembers.prototype.loadData = function () {
        var _this = this;
        this.l2nHttp.getRequest('users-list', 'loading data...').then(function (data) {
            _this.loadedList = data;
        }, function (err) { });
    };
    ListOfMembers.prototype.onRemoveToSelectList = function (member) {
        this.memberManagementProvider.removeToSelectedMember(member);
    };
    ListOfMembers.prototype.isSelected = function (member) {
        return this.memberManagementProvider.isMemberSelected(member);
    };
    ListOfMembers.prototype.onAddToSelectList = function (member) {
        this.memberManagementProvider.addToSelectedMember(member);
    };
    return ListOfMembers;
}());
ListOfMembers = __decorate([
    ionic_angular_1.IonicPage(),
    core_1.Component({
        selector: 'page-list-of-members',
        templateUrl: 'list-of-members.html',
    })
], ListOfMembers);
exports.ListOfMembers = ListOfMembers;

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListOfMembers } from './list-of-members';

@NgModule({
  declarations: [
    ListOfMembers,
  ],
  imports: [
    IonicPageModule.forChild(ListOfMembers),
  ],
  exports: [
    ListOfMembers
  ]
})
export class ListOfMembersModule {}

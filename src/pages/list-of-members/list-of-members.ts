import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {L2nHttp} from "../../providers/l2n-http";
import {API_CONFIG} from "../../constants/api";
import {MemberManagementProvider} from "../../providers/memberManagement";
import {Member} from "../../mapper/member.interface";
import {BlockUi} from "../../providers/block-ui";

@IonicPage()
@Component({
  selector: 'page-list-of-members',
  templateUrl: 'list-of-members.html',
})
export class ListOfMembers {

  private loadedList: any =[];
  private imageUrl:any;
  home = 'Home';
  constructor(private l2nHttp:L2nHttp,
              private memberManagementProvider:MemberManagementProvider,
              private blockUi: BlockUi,
              public navCtrl: NavController) {
    this.imageUrl =API_CONFIG.misBase;
  }

  ionViewDidLoad() {
    this.loadData();
    console.log()
  }

  private loadData() {
    this.l2nHttp.getRequest('users-list','loading data...').then(
      data => {
        this.loadedList = data;
      },
      err => {}
    );
  }

  standing(){
    this.navCtrl.push('home-page');
  }

  onRemoveToSelectList(member: Member){
    this.memberManagementProvider.removeToSelectedMember(member,1);
  }

  isSelected(member: Member){
    return this.memberManagementProvider.isMemberSelected(member,1);
  }

  onAddToSelectList(member:Member){
    //console.log(this.memberManagementProvider.getSelectedMember().length);

    if(this.memberManagementProvider.getSelectedMember().length <=6){
      this.memberManagementProvider.addToSelectedMember(member,1,1);
    }
    else{
      this.blockUi.alert('You can not select more then 7 user','User Attention!');
    }
  }

}

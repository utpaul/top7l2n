import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListOfTeamLeaderMembers } from './list-of-team-leader-list';

@NgModule({
  declarations: [
    ListOfTeamLeaderMembers,
  ],
  imports: [
    IonicPageModule.forChild(ListOfTeamLeaderMembers),
  ],
  exports: [
    ListOfTeamLeaderMembers
  ]
})
export class ListOfTeamMembersModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListOfTeamMembers } from './list-of-team-member';

@NgModule({
  declarations: [
    ListOfTeamMembers,
  ],
  imports: [
    IonicPageModule.forChild(ListOfTeamMembers),
  ],
  exports: [
    ListOfTeamMembers
  ]
})
export class ListOfTeamMembersModule {}

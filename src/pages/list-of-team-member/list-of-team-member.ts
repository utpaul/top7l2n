import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {API_CONFIG} from "../../constants/api";
import {L2nHttp} from "../../providers/l2n-http";
import {Member} from "../../mapper/member.interface";
import {MemberManagementProvider} from "../../providers/memberManagement";
import {BlockUi} from "../../providers/block-ui";
import {User} from "../../mapper/user";
import {AuthProvider} from "../../providers/auth";

@IonicPage()
@Component({
  selector: 'list-of-team-member',
  templateUrl: 'list-of-team-member.html',
})
export class ListOfTeamMembers {

  private loadedList: any =[];
  private imageUrl:any;
  user: User;

  constructor(private l2nHttp:L2nHttp,
              private memberManagementProvider:MemberManagementProvider,
              private blockUi: BlockUi,
              private auth:AuthProvider,
              public navCtrl: NavController) {
    this.imageUrl =API_CONFIG.misBase;
  }

  standing(){
    this.navCtrl.push('home-page');
  }

  ionViewDidLoad() {
    this.loadData();
  }

  private loadData() {
    this.l2nHttp.getRequest('dept-user','loading data...').then(
      data => {
        this.loadedList = data;
      },
      err => {}
    );
  }

  onRemoveToSelectList(member: Member){
    this.memberManagementProvider.removeToSelectedMember(member,2);
  }

  isSelected(member: Member){
    return this.memberManagementProvider.isMemberSelected(member,2);
  }

  onAddToSelectList(member:Member){
    this.user = new User(this.auth.currentUser);

    if(this.memberManagementProvider.getSelectedTeamMember().length < parseInt(this.user.calculation)){
      this.memberManagementProvider.addToSelectedMember(member,2,1);
    }
    else{
      this.blockUi.alert('You can not select more then 3 user','User Attention!');
    }

  }


}

import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {BlockUi} from "../../providers/block-ui";
import {NetworkConnectionProvider} from "../../providers/networkConnection";
import {AuthProvider} from "../../providers/auth";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {L2nHttp} from "../../providers/l2n-http";

@IonicPage({
  name: 'login-page',
  priority: 'high'
})

@Component({
  selector: 'login-page',
  templateUrl: 'login-page.html',
})

export class Login {

  isOnline: boolean = false;
  profilePicture: any = "assets/logo.png";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public platform: Platform,
              private blockUi: BlockUi,
              private networkCheck:NetworkConnectionProvider,
              private auth: AuthProvider,
              private screenOrientation: ScreenOrientation,
              private l2nHttp:L2nHttp) {

    this.platform.ready().then(() => {
      this.isOnline =this.networkCheck.isOnline;

      if(platform.is('core')){

      } else if(platform.is('android')){
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      }
    });

  }

  emailChanged(value){

    this.l2nHttp.getFirstRequest('getting-image/'+value,'getting image...')
      .then(
      data => {
        console.log(data);
        this.profilePicture = "http://l2nsoft.ml/mis.zxyinternational.com/top7l2n/"+data;
      },
      err => {

      }
    );
  }

  login(FormLogin){

    this.platform.ready().then(() => {

      let a =this.blockUi.show('Signing you in...');

      this.auth.login(FormLogin.value)

        .then(data =>{
          FormLogin.reset();
          a.dismiss();
        })

        .catch(error =>{
          FormLogin.reset();
          a.dismiss();
          this.blockUi.alert('Sorry! Invalid Username or Password', 'Sign In Problem!');
      })

    })
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectXnmember } from './select-xnmember';

@NgModule({
  declarations: [
    SelectXnmember,
  ],
  imports: [
    IonicPageModule.forChild(SelectXnmember),
  ],
  exports: [
    SelectXnmember
  ]
})
export class SelectXnmemberModule {}

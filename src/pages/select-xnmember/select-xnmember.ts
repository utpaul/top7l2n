import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'list-of-team-member',
  templateUrl: 'select-xnmember.html',
})
export class SelectXnmember {

  tab1:any;
  tab2:any;

  constructor() {
    this.tab1='ListOfTeamMembers';
    this.tab2='SelectedTeamMembers';
  }

}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var Select7member = (function () {
    function Select7member() {
        this.tab1 = 'ListOfMembers';
        this.tab2 = 'SelectedL2nMembers';
    }
    return Select7member;
}());
Select7member = __decorate([
    ionic_angular_1.IonicPage(),
    core_1.Component({
        selector: 'page-select7member',
        templateUrl: 'select7member.html',
    })
], Select7member);
exports.Select7member = Select7member;

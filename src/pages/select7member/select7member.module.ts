import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Select7member } from './select7member';

@NgModule({
  declarations: [
    Select7member,
  ],
  imports: [
    IonicPageModule.forChild(Select7member),
  ],
  exports: [
    Select7member
  ]
})
export class Select7memberModule {}

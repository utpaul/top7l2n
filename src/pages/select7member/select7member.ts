import { Component } from '@angular/core';
import { IonicPage} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-select7member',
  templateUrl: 'select7member.html',
})
export class Select7member {

  tab1:any;
  tab2:any;

  constructor() {
    this.tab1='ListOfMembers';
    this.tab2='SelectedL2nMembers';
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedAdminMembers } from './selected-admin-members';

@NgModule({
  declarations: [
    SelectedAdminMembers,
  ],
  imports: [
    IonicPageModule.forChild(SelectedAdminMembers),
  ],
  exports: [
    SelectedAdminMembers
  ]
})
export class SelectedTeamMembersModule {}

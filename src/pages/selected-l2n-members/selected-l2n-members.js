"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var api_1 = require("../../constants/api");
var SelectedL2nMembers = (function () {
    function SelectedL2nMembers(memberManagementProvider) {
        this.memberManagementProvider = memberManagementProvider;
        this.imageUrl = api_1.API_CONFIG.misBase;
    }
    SelectedL2nMembers.prototype.ionViewWillEnter = function () {
        console.log('enter in selected l2n member');
        this.memberLists = this.memberManagementProvider.getSelectedMember();
        console.log(this.memberLists);
    };
    SelectedL2nMembers.prototype.onRemoveFromSelectedList = function (member) {
        this.memberManagementProvider.removeToSelectedMember(member, 1);
        var position = this.memberLists.findIndex(function (quoteEl) {
            return quoteEl.id == member.id;
        });
        this.memberLists.splice(position, 1);
    };
    return SelectedL2nMembers;
}());
SelectedL2nMembers = __decorate([
    ionic_angular_1.IonicPage(),
    core_1.Component({
        selector: 'page-selected-l2n-members',
        templateUrl: 'selected-l2n-members.html',
    })
], SelectedL2nMembers);
exports.SelectedL2nMembers = SelectedL2nMembers;

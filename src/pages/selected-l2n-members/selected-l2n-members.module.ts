import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedL2nMembers } from './selected-l2n-members';

@NgModule({
  declarations: [
    SelectedL2nMembers,
  ],
  imports: [
    IonicPageModule.forChild(SelectedL2nMembers),
  ],
  exports: [
    SelectedL2nMembers
  ]
})
export class SelectedL2nMembersModule {}

import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {MemberManagementProvider} from "../../providers/memberManagement";
import {Member} from "../../mapper/member.interface";
import {API_CONFIG} from "../../constants/api";
import {L2nHttp} from "../../providers/l2n-http";

@IonicPage()
@Component({
  selector: 'page-selected-l2n-members',
  templateUrl: 'selected-l2n-members.html',
})
export class SelectedL2nMembers {

  memberLists: Member[];
  private imageUrl:any;
  private memberListsLength:number;

  constructor(private memberManagementProvider:MemberManagementProvider,
              public navCtrl: NavController,
              private l2nHttp:L2nHttp) {
    this.imageUrl =API_CONFIG.misBase;
  }

  standing(){
    this.navCtrl.push('home-page');
  }

  postingData(){

    this.l2nHttp.post('l2n-member',
      this.memberManagementProvider.getSelectedMember(),'Saving data').then(
      data => {
        console.log(data)
      },
      err => {}
    );
  }

  ionViewWillEnter() {
    console.log('enter in selected l2n member');
    this.memberLists = this.memberManagementProvider.getSelectedMember();
    this.memberListsLength = this.memberLists.length;
  }

  onRemoveFromSelectedList(member: Member){

    this.memberManagementProvider.removeToSelectedMember(member,1);

    const position = this.memberLists.findIndex((quoteEl: Member) =>{
      return quoteEl.id == member.id;
    })

    this.memberLists.splice(position, 1);
    this.memberListsLength--;
  }

}

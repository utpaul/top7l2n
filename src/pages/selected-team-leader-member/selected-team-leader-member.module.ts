import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedTeamLeaderMembers } from './selected-team-leader-member';

@NgModule({
  declarations: [
    SelectedTeamLeaderMembers,
  ],
  imports: [
    IonicPageModule.forChild(SelectedTeamLeaderMembers),
  ],
  exports: [
    SelectedTeamLeaderMembers
  ]
})
export class SelectedTeamMembersModule {}

import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {API_CONFIG} from "../../constants/api";
import {MemberManagementProvider} from "../../providers/memberManagement";
import {MemberPoints} from "../../mapper/memberPoints.interface";
import {BlockUi} from "../../providers/block-ui";
import {L2nHttp} from "../../providers/l2n-http";

@IonicPage()
@Component({
  selector: 'selected-team-leader-member',
  templateUrl: 'selected-team-leader-member.html',
})
export class SelectedTeamLeaderMembers {

  memberLists: MemberPoints[];
  private imageUrl:any;
  removeFlag:boolean=true;
  private counter: number = 0;

  constructor(private memberManagementProvider:MemberManagementProvider,
              private blockUi: BlockUi,
              public navCtrl: NavController,
              private l2nHttp:L2nHttp) {
    this.imageUrl =API_CONFIG.misBase;
  }

  standing(){
    this.navCtrl.push('home-page');
  }

  postingData(){
    this.l2nHttp.post('tl-points-list',
      this.memberManagementProvider.getSelectedTLPointsMember(),'Saving data').then(
      data => {
        console.log(data)
      },
      err => {}
    );
  }

  ionViewWillEnter() {
    console.log('enter in selected l2n member');
    this.memberLists = this.memberManagementProvider.getSelectedTLPointsMember();
  }

  onRemoveFromSelectedList(member: MemberPoints){

    this.memberManagementProvider.removeToSelectedSpecialMember(member,1);
    this.memberLists = this.memberManagementProvider.getSelectedTLPointsMember();
  }

  onAddToSelectList(member:MemberPoints){

    this.memberLists =this.memberManagementProvider.getSelectedTLPointsMember();

    if(this.memberLists.length > 0){

      for(let i=0;i<this.memberLists.length;i++){
        this.counter +=this.memberLists[i].points;
      }

    } else{
      this.counter++;
    }

    console.log(this.counter);

    if(this.counter < 2){
      member.points++;
      console.log('member points'+ member.points);
      this.memberManagementProvider.addToSelectedMember(member,3,member.points);
    }else {
      this.blockUi.alert('You can not select more then 5 user','User Attention!');
    }
    this.counter=0;
  }

}

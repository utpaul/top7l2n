import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedTeamMembers } from './selected-team-member';

@NgModule({
  declarations: [
    SelectedTeamMembers,
  ],
  imports: [
    IonicPageModule.forChild(SelectedTeamMembers),
  ],
  exports: [
    SelectedTeamMembers
  ]
})
export class SelectedTeamMembersModule {}

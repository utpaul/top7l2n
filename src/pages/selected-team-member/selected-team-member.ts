import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {Member} from "../../mapper/member.interface";
import {API_CONFIG} from "../../constants/api";
import {MemberManagementProvider} from "../../providers/memberManagement";
import {L2nHttp} from "../../providers/l2n-http";
import {AuthProvider} from "../../providers/auth";
import {User} from "../../mapper/user";

@IonicPage()
@Component({
  selector: 'selected-team-member',
  templateUrl: 'selected-team-member.html',
})
export class SelectedTeamMembers {

  memberLists: Member[];
  private imageUrl:any;
  private memberListsLength:number;
  private totalTeamMember:number;
  user: User;

  constructor(private memberManagementProvider:MemberManagementProvider,
              public navCtrl: NavController,
              private l2nHttp:L2nHttp,
              private auth:AuthProvider) {
    this.imageUrl =API_CONFIG.misBase;
    this.user = new User(this.auth.currentUser);
  }

  standing(){
    this.navCtrl.push('home-page');
  }

  postingData(){
    this.l2nHttp.post('team-member-list',
      this.memberManagementProvider.getSelectedTeamMember(),'Saving data').then(
      data => {
        console.log(data)
      },
      err => {}
    );
  }

  ionViewWillEnter() {
    this.totalTeamMember =parseInt(this.user.calculation);
    console.log('enter in selected l2n member');
    this.memberLists = this.memberManagementProvider.getSelectedTeamMember();
    this.memberListsLength = this.memberLists.length;
  }

  onRemoveFromSelectedList(member: Member){

    this.memberManagementProvider.removeToSelectedMember(member,2);

    const position = this.memberLists.findIndex((quoteEl: Member) =>{
      return quoteEl.id == member.id;
    })

    this.memberLists.splice(position, 1);
    this.memberListsLength--;
  }

}

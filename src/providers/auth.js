"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var localforage_1 = require("localforage");
var user_1 = require("../mapper/user");
var _1 = require("../constants/");
var angular2_jwt_1 = require("angular2-jwt");
var api_1 = require("../constants/api");
var AuthProvider = AuthProvider_1 = (function () {
    function AuthProvider(http, platform) {
        var _this = this;
        this.platform = platform;
        this._onSessionStore = new core_1.EventEmitter();
        this._isAuthenticated = false;
        this._loaded = false;
        this.baseUrl = '/api/';
        this.jwtHelper = new angular2_jwt_1.JwtHelper();
        this.contentHeader = new http_1.Headers({ 'X-API-KEY': [api_1.API_CONFIG['X-API-KEY']],
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        this._http = http;
        this.platform.ready().then(function () {
            if (platform.is('core')) {
                _this.baseUrl = '/api/';
            }
            else if (platform.is('android')) {
                _this.baseUrl = api_1.API_CONFIG.baseUrl;
            }
            _this.loadUserCredentials();
        });
    }
    AuthProvider.prototype.ready = function () {
        var _this = this;
        if (this._loaded) {
            return Promise.resolve();
        }
        return new Promise(function (resolve) {
            var a = _this.onSessionStore.subscribe(function () {
                resolve();
                a.unsubscribe();
            });
        });
    };
    Object.defineProperty(AuthProvider.prototype, "http", {
        get: function () {
            return this._http;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthProvider.prototype, "authToken", {
        get: function () {
            return this._authToken;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthProvider.prototype, "isAuthenticated", {
        get: function () {
            return this._isAuthenticated;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthProvider.prototype, "onSessionStore", {
        get: function () {
            return this._onSessionStore;
        },
        enumerable: true,
        configurable: true
    });
    AuthProvider.prototype.loadUserCredentials = function () {
        var _this = this;
        localforage_1.default.getItem(_1.LOCAL_TOKEN_KEY)
            .then(function (data) { return _this.useCredentials(data); }, function (error) { console.error(error); _this.destroyUserCredentials(); });
    };
    AuthProvider.prototype.useCredentials = function (token) {
        this._authToken = token;
        this._currentUser = this.getClaimsFromToken(token);
        this._isAuthenticated = this._currentUser != null;
        this._loaded = true;
        this.onSessionStore.emit();
    };
    AuthProvider.prototype.getClaimsFromToken = function (token) {
        var user;
        if (typeof token !== 'undefined' && token != null) {
            var payload = this.jwtHelper.decodeToken(token);
            user = payload.user;
            console.log(user);
            user.profile = AuthProvider_1.getProfileImageUrl(user.profile);
            console.log(user.profile);
        }
        return user;
    };
    AuthProvider.prototype.storeUserCredentials = function (token) {
        if (token == null) {
            this.destroyUserCredentials();
        }
        else {
            localforage_1.default.setItem(_1.LOCAL_TOKEN_KEY, token)
                .then(function (data) { return console.log(data); }, function (error) { return console.error(error); });
            this.useCredentials(token);
        }
    };
    AuthProvider.getProfileImageUrl = function (profile) {
        if (typeof profile == 'undefined') {
            return;
        }
        return api_1.API_CONFIG.misBase + profile;
    };
    AuthProvider.prototype.destroyUserCredentials = function () {
        var _this = this;
        console.log('destroying');
        this._authToken = undefined;
        this._currentUser = new user_1.User({});
        this._isAuthenticated = false;
        localforage_1.default.removeItem(_1.LOCAL_TOKEN_KEY)
            .then(function (data) { _this.onSessionStore.emit(); }, function (error) { return console.error(error); });
    };
    Object.defineProperty(AuthProvider.prototype, "currentUser", {
        get: function () {
            return this._currentUser;
        },
        enumerable: true,
        configurable: true
    });
    AuthProvider.prototype.login = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log(_this.baseUrl);
            _this._http.post(_this.baseUrl + 'access-token', "email=" + data.username +
                "&password=" + data.password, { headers: _this.contentHeader })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data.token);
                _this.authSuccess(data.token);
                resolve('Login success.');
            }, function (err) {
                _this.error = err;
                console.log(err);
                reject('Login Failed.');
            });
        });
    };
    AuthProvider.prototype.authSuccess = function (token) {
        this.error = null;
        this.storeUserCredentials(token);
    };
    return AuthProvider;
}());
AuthProvider = AuthProvider_1 = __decorate([
    core_1.Injectable()
], AuthProvider);
exports.AuthProvider = AuthProvider;
var AuthProvider_1;

import {Injectable, EventEmitter} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import localForage from "localforage";
import {User} from "../mapper/user";
import {LOCAL_TOKEN_KEY} from "../constants/";
import {Platform} from "ionic-angular";
import {JwtHelper} from "angular2-jwt";
import {API_CONFIG} from "../constants/api";

@Injectable()
export class AuthProvider {

  private _onSessionStore = new EventEmitter<any>();
  private _currentUser:User;
  private _isAuthenticated = false;
  private _authToken:string;
  private error:string;
  private _http: Http;
  private _loaded = false;

  private baseUrl:string ='/api/';
  private jwtHelper: JwtHelper = new JwtHelper();

  contentHeader: Headers = new Headers({'X-API-KEY': [API_CONFIG['X-API-KEY']],
    'Content-Type': 'application/x-www-form-urlencoded'
  });
  constructor(http: Http, public platform: Platform) {
    this._http = http;
    this.platform.ready().then(() => {

      if(platform.is('core')){
        this.baseUrl ='/api/';
      } else if(platform.is('android')){
        this.baseUrl = API_CONFIG.baseUrl;
      }

      this.loadUserCredentials();
    });

  }

  ready():Promise<any> {

    if(this._loaded) {
      return Promise.resolve();
    }
    return new Promise((resolve)=>{
      let a = this.onSessionStore.subscribe(()=>{
        resolve();
        a.unsubscribe();
      });
    });

  }

  get http() {
    return this._http;
  }

  get authToken(): string {
    return this._authToken;
  }

  get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }

  get onSessionStore(): EventEmitter<any> {
    return this._onSessionStore;
  }

  loadUserCredentials() {
    localForage.getItem(LOCAL_TOKEN_KEY)
      .then(
        data => this.useCredentials(data),
        error => {console.error(error);  this.destroyUserCredentials();}
      );
  }

  useCredentials(token) {
    this._authToken = token;
    this._currentUser = this.getClaimsFromToken(token);
    this._isAuthenticated = this._currentUser != null;
    this._loaded = true;
    this.onSessionStore.emit();
  }

  getClaimsFromToken(token):User {
    let user:User;

    if (typeof token !== 'undefined' && token != null) {
      let payload = this.jwtHelper.decodeToken(token);
      user = payload.user;
      console.log(user);
      user.profile = AuthProvider.getProfileImageUrl(user.profile);
      console.log(user.profile);
    }
    return user;
  }

  storeUserCredentials(token) {
    if (token == null) {
      this.destroyUserCredentials();
    } else {
      localForage.setItem(LOCAL_TOKEN_KEY, token)
        .then(
          data => console.log(data),
          error => console.error(error)
        );

      this.useCredentials(token);
    }
  }

  static getProfileImageUrl(profile) {
    if(typeof profile == 'undefined') {
      return;
    }
    return API_CONFIG.misBase + profile;
  }

  destroyUserCredentials() {

    console.log('destroying');

    this._authToken = undefined;
    this._currentUser = new User({});
    this._isAuthenticated = false;
    localForage.removeItem(LOCAL_TOKEN_KEY)
      .then(
        data => {this.onSessionStore.emit();},
        error => console.error(error)
      );
  }


  get currentUser(): User {
    return this._currentUser;
  }


  login(data): Promise<any> {
    return new Promise((resolve, reject) => {
      console.log(this.baseUrl);
      this._http.post(this.baseUrl + 'access-token',
        "email=" + data.username +
        "&password=" + data.password,
        { headers: this.contentHeader })
        .map(res => res.json())
        .subscribe(
          data => {
            console.log(data.token);
            this.authSuccess(data.token);
            resolve('Login success.');
          },
          err => {this.error = err;
            console.log(err);
          reject('Login Failed.');}
        );
    });
  }

  authSuccess(token) {
    this.error = null;
    this.storeUserCredentials(token);
  }
}

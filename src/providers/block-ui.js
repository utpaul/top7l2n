"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var BlockUi = (function () {
    function BlockUi(loadingCtrl, _alert) {
        this.loadingCtrl = loadingCtrl;
        this._alert = _alert;
        this.defaultContent = "Processing...";
    }
    BlockUi.prototype.show = function (msg) {
        this.loading = this.loadingCtrl.create({
            spinner: 'circles',
            content: msg || this.defaultContent
        });
        this.loading.present().then(function (s) { return console.log(s); }, function (e) { return console.error(e); });
    };
    BlockUi.prototype.hide = function () {
        var _this = this;
        if (this.loading == null) {
            return;
        }
        this.loading.dismiss().then(function () { return _this.loading = null; }, function () { return _this.loading = null; });
    };
    BlockUi.prototype.alert = function (msg, title) {
        var alert = this._alert.create({
            title: title,
            message: msg,
            buttons: ['Dismiss'],
            cssClass: "danger-alert"
        });
        alert.present().then(function (s) { return console.log(s); }, function (e) { return console.error(e); });
    };
    BlockUi.prototype.error = function (msg) {
        return this.alert(msg, "Error!!");
    };
    return BlockUi;
}());
BlockUi = __decorate([
    core_1.Injectable()
], BlockUi);
exports.BlockUi = BlockUi;

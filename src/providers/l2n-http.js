"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/add/operator/map");
var http_1 = require("@angular/http");
var api_1 = require("../constants/api");
var L2nHttp = (function () {
    function L2nHttp(blockUi, _auth, platform) {
        var _this = this;
        this.blockUi = blockUi;
        this._auth = _auth;
        this.platform = platform;
        this.baseUrl = '/api/';
        this.platform.ready().then(function () {
            if (platform.is('core')) {
                _this.baseUrl = '/api/';
            }
            else if (platform.is('android')) {
                _this.baseUrl = api_1.API_CONFIG.baseUrl;
            }
        });
    }
    Object.defineProperty(L2nHttp.prototype, "http", {
        get: function () {
            return this._auth.http;
        },
        enumerable: true,
        configurable: true
    });
    L2nHttp.prototype.getRequest = function (url, preMessage) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.blockUi.show(preMessage);
            _this.platform.ready().then(function () {
                _this.http.get(_this.baseUrl + url, { headers: new http_1.Headers({
                        "X-Auth-Token": _this._auth.authToken,
                        'X-API-KEY': api_1.API_CONFIG['X-API-KEY']
                    }) }).map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    resolve(data);
                    _this.blockUi.hide();
                }, function (err) {
                    reject(err);
                    _this.blockUi.hide();
                });
            });
        });
    };
    return L2nHttp;
}());
L2nHttp = __decorate([
    core_1.Injectable()
], L2nHttp);
exports.L2nHttp = L2nHttp;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MemberManagementProvider = (function () {
    function MemberManagementProvider() {
        this.selectedMember = [];
        this.selectedTeamMember = [];
    }
    MemberManagementProvider.prototype.addToSelectedMember = function (member, flag) {
        if (flag == 1) {
            this.selectedMember.push(member);
            console.log(this.selectedMember);
        }
        else if (flag == 2) {
            this.selectedTeamMember.push(member);
            console.log(this.selectedMember);
        }
    };
    MemberManagementProvider.prototype.removeToSelectedMember = function (member, flag) {
        if (flag == 1) {
            var position = this.selectedMember.findIndex(function (memberEl) {
                return memberEl.id == member.id;
            });
            this.selectedMember.splice(position, 1);
            console.log(this.selectedMember);
        }
        else if (flag == 2) {
            var position = this.selectedTeamMember.findIndex(function (memberEl) {
                return memberEl.id == member.id;
            });
            this.selectedTeamMember.splice(position, 1);
            console.log(this.selectedTeamMember);
        }
    };
    MemberManagementProvider.prototype.getSelectedMember = function () {
        return this.selectedMember.slice();
    };
    MemberManagementProvider.prototype.getSelectedTeamMember = function () {
        return this.selectedTeamMember.slice();
    };
    MemberManagementProvider.prototype.isMemberSelected = function (member, flag) {
        if (flag == 1) {
            return this.selectedMember.find(function (memberEl) {
                return memberEl.id == member.id;
            });
        }
        else if (flag == 2) {
            return this.selectedTeamMember.find(function (memberEl) {
                return memberEl.id == member.id;
            });
        }
    };
    return MemberManagementProvider;
}());
exports.MemberManagementProvider = MemberManagementProvider;

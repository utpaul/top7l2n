import {Member} from "../mapper/member.interface";
import {MemberPoints} from "../mapper/memberPoints.interface";

export class MemberManagementProvider{

  private selectedMember: Member[] =[];
  private selectedTeamMember: Member[] =[];
  private selectedTLPointsMember: MemberPoints[] =[];
  private selectedAdminPointsMember: MemberPoints[] =[];
  private converter:any;

  addToSelectedMember(member: Member,
                      flag:number,
                      point:number){
    if(flag == 1){
      this.selectedMember.push(member);
      console.log(this.selectedMember);
    }
    else if(flag == 2){
      this.selectedTeamMember.push(member);
      console.log(this.selectedTeamMember);
    }

    else if(flag == 3){
      this.converter=member;
      this.converter.points =point;

      if(this.converter.points > 1){

        for(let i=0;i<this.selectedTLPointsMember.length;i++){

          if(this.selectedTLPointsMember[i].email == this.converter.email){
            this.selectedTLPointsMember[i].points = this.converter.points;
          }
        }
      }else {
        this.selectedTLPointsMember.push(this.converter);
      }

      console.log(this.selectedTLPointsMember);
    }else if(flag == 4){

      this.converter=member;
      this.converter.points =point;
      console.log('points in service'+ this.converter.points);

      if(this.converter.points > 1){

        for(let i=0;i<this.selectedAdminPointsMember.length;i++){

          if(this.selectedAdminPointsMember[i].email == this.converter.email){
            this.selectedAdminPointsMember[i].points = this.converter.points;
          }
        }

      }else {
        this.selectedAdminPointsMember.push(this.converter);
      }
    }

  }


  removeToSelectedMember(member: Member,
                         flag:number){
    if(flag == 1){
      const position = this.selectedMember.findIndex((memberEl: Member) =>{
        return memberEl.id == member.id;
      })
      this.selectedMember.splice(position, 1);
      console.log(this.selectedMember);
    }

    else if(flag == 2){

      const position = this.selectedTeamMember.findIndex((memberEl: Member) =>{
        return memberEl.id == member.id;
      })
      this.selectedTeamMember.splice(position, 1);
      console.log(this.selectedTeamMember);
    }

    else if(flag == 3){
      const position = this.selectedTLPointsMember.findIndex((memberEl: Member) =>{
        return memberEl.id == member.id;
      })
      this.selectedTLPointsMember.splice(position, 1);
      console.log(this.selectedTLPointsMember);
    }

  }

  removeToSelectedSpecialMember(member:MemberPoints,flag:number) {

    if(flag == 1 && this.isSpecialMemberSelected(member)){

      for(let i=0;i<this.selectedTLPointsMember.length;i++){

        if(this.selectedTLPointsMember[i].points == 1){

          const position = this.selectedTLPointsMember.findIndex((memberEl: Member) =>{
            return memberEl.id == member.id;
          });

          this.selectedTLPointsMember.splice(position, 1);
        }
        else{
          this.selectedTLPointsMember[i].points--;
        }
      }
    }else if(flag == 2){

      for(let i=0;i<this.selectedAdminPointsMember.length;i++){

        if(this.selectedAdminPointsMember[i].id == member.id){

          const position = this.selectedAdminPointsMember.findIndex((memberEl: Member) =>{
            return memberEl.id == member.id;
          });

          if(member.points>1){
            this.selectedAdminPointsMember[i].points--;
          }else {
            this.selectedAdminPointsMember.splice(position,1);
          }

          console.log(this.selectedAdminPointsMember);

        }
      }

    }

  }

  getSelectedMember(){
    return this.selectedMember.slice();
  }

  getSelectedTeamMember(){
    return this.selectedTeamMember.slice();
  }

  getSelectedTLPointsMember(){
    return this.selectedTLPointsMember.slice();
  }

  getSelectedAdminPointsMember(){
    return this.selectedAdminPointsMember.slice();
  }

  isMemberSelected(member: Member,
                   flag:number) {
    if(flag == 1){
      return this.selectedMember.find((memberEl: Member) => {
        return memberEl.id == member.id;
      });
    }
    else if(flag == 2){
      return this.selectedTeamMember.find((memberEl: Member) => {
        return memberEl.id == member.id;
      });
    }

    else if(flag == 3){
      return this.selectedTLPointsMember.find((memberEl: Member) => {
        return memberEl.id == member.id;
      });
    }
  }

  isSpecialMemberSelected(member: MemberPoints){

    return (this.selectedTLPointsMember.findIndex((quoteEl: Member) =>{
      return quoteEl.id == member.id;
    }) + 1);
  }


  removeAllLists(){

    this.selectedMember =[];
    this.selectedTeamMember =[];
    this.selectedTLPointsMember=[];
    this.selectedAdminPointsMember =[];
  }


  //
  // setSelectedTeamMember(){
  //
  //   this.l2nHttp.getRequest('get-team-members','loading data...').then(
  //     data => {
  //       for(let i=0 ; i<data.length; i++){
  //         this.selectedTeamMember.push(data[i]);
  //       }
  //     },
  //     err => {}
  //   );
  // }
  //
  // setSelectedTLPointsMember(){
  //
  //   this.l2nHttp.getRequest('get-tl-points-members','loading data...').then(
  //     data => {
  //       for(let i=0 ; i<data.length; i++){
  //         this.selectedTLPointsMember.push(data[i]);
  //       }
  //     },
  //     err => {}
  //   );
  //
  // }
  //
  // setSelectedAdminPointsMember(){
  //
  //   this.l2nHttp.getRequest('get-admin-points-members','loading data...').then(
  //     data => {
  //       for(let i=0 ; i<data.length; i++){
  //         this.selectedAdminPointsMember.push(data[i]);
  //       }
  //     },
  //     err => {}
  //   );
  //
  // }


}

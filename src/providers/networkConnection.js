"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ConnectionStatusEnum;
(function (ConnectionStatusEnum) {
    ConnectionStatusEnum[ConnectionStatusEnum["Online"] = 0] = "Online";
    ConnectionStatusEnum[ConnectionStatusEnum["Offline"] = 1] = "Offline";
})(ConnectionStatusEnum = exports.ConnectionStatusEnum || (exports.ConnectionStatusEnum = {}));
var NetworkConnectionProvider = (function () {
    function NetworkConnectionProvider(network, platform, toastCtrl, eventCtrl) {
        var _this = this;
        this.network = network;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.eventCtrl = eventCtrl;
        this.isOnline = true;
        this.platform.ready().then(function () {
            _this.previousStatus = ConnectionStatusEnum.Online;
            _this.initializeNetworkEvents();
        });
    }
    NetworkConnectionProvider.prototype.displayNetworkUpdate = function (connectionState) {
        var networkType = this.network.type;
        this.toastCtrl.create({
            message: 'You are now ' + connectionState + ' via ' + networkType,
            duration: 3000
        }).present();
    };
    NetworkConnectionProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.previousStatus === ConnectionStatusEnum.Online) {
                _this.eventCtrl.publish('network:offline');
                _this.displayNetworkUpdate(_this.network.type);
            }
            _this.previousStatus = ConnectionStatusEnum.Offline;
            _this.isOnline = false;
        });
        this.network.onConnect().subscribe(function () {
            if (_this.previousStatus === ConnectionStatusEnum.Offline) {
                _this.eventCtrl.publish('network:online');
                _this.displayNetworkUpdate(_this.network.type);
            }
            _this.previousStatus = ConnectionStatusEnum.Online;
            _this.isOnline = true;
        });
    };
    return NetworkConnectionProvider;
}());
NetworkConnectionProvider = __decorate([
    core_1.Injectable()
], NetworkConnectionProvider);
exports.NetworkConnectionProvider = NetworkConnectionProvider;
